'use strict';

angular.module('ap.form', ['ng'])

  .filter('isEmpty', [function() {
    return function(object) {
      if(object === undefined) {return false;}
      return Object.keys(object).length;
    };
  }])

  .service('$formService', function(){
    return {f: {}, schemas: {}};
  })

  .factory('$stateChange', function($rootScope, $modal, $location, $state) {

    var eventFunc;

    var ModalSaveCtrl = function ($scope, $modalInstance) {
      $scope.close = function(todo){
        $modalInstance.close(todo);
      };

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };
    };

    return {
      on: function(cb){

        eventFunc = $rootScope.$on('$stateChangeStart', function(event, toState, toParams){
          event.preventDefault();

          var modalSave = $modal.open({
            templateUrl: 'bower_components/apform/templates/statechange.modal.html',
            controller: ModalSaveCtrl
          });

          modalSave.result.then(function(todo){
            if(todo === 'notSave'){
              eventFunc();
              $state.go(toState.name, toParams);
            }

            cb(todo, toState, toParams);
          });
        });
      },

      off: function(){
        eventFunc();
      }
    };
  })

  .directive('scrollToActive', function($timeout) {
    return {
      restrict: 'A',
      scope: false,
      link: function($scope, $element){

        var scrollToActive = function(){
          if($element[0].childNodes.length > 0){
            deregister();
            $timeout(function(){
              var li = $element.children('ul').eq(0).children('li.active');
              var offset = 0;
              if (li[0]) { offset = li[0].offsetTop; }
              $element[0].scrollTop = offset;
            });
          }
        };
        var deregister = $scope.$watch(function () { return $element[0].children[0].childNodes.length; }, scrollToActive );
      }
    };
  })


  .directive('hideScroll', function() {
    return {
      restrict: 'A',
      scope: false,
      template: '<div class="hidescroll-content" ng-transclude></div>',
      transclude: true,
      link: function($scope, $element){
        $element.addClass('hidescroll-container');
        $element[0].childNodes[0].style.right = $element[0].childNodes[0].clientWidth - $element[0].childNodes[0].offsetWidth+'px';
      }
    };
  })

  .directive('apform', function($parse, $log, $formService) {
    return {
      restrict: 'E',
      scope: false,
      templateUrl: 'bower_components/apform/templates/apform.html',
      replace: true,
      transclude: true,
      link: function(scope){
        scope.f = $formService.f;

        scope.$watch('apform.$valid', function() {
          scope.f.valid = scope.apform.$valid;
        });
      }
    };
  })

  .directive('aptile', function() {
    return {
      restrict: 'E',
      scope: {
        tileTitle: '@',
        tileNumber: '@',
        tileIcon: '@',
        tileMore: '@',
        tileChange: '='
      },
      templateUrl: 'bower_components/apform/templates/aptile.html',
      transclude: true,
      replace: true,
      link: function($scope){

        if($scope.tileMore){
          $scope.showmore = true;
          $scope.morecontent = false;
        }else{
          $scope.showmore = false;
        }

        $scope.changeContent = function(val){
          $scope.morecontent = val;
          $scope.tileChange = val;
        };
      }
    };
  })

  .directive('aptilefilter', function() {
    return {
      restrict: 'E',
      scope: {
        tileTitle: '@',
        tileNumber: '@',
        tileIcon: '@',
        tileFilter: '='
      },
      templateUrl: 'bower_components/apform/templates/aptilefilter.html',
      transclude: true,
      replace: true
    };
  })

  .directive('aptileclear', function() {
    return {
      restrict: 'E',
      scope: {
        height: '='
      },
      templateUrl: 'bower_components/apform/templates/aptileclear.html',
      transclude: true,
      replace: true
    };
  })

  .directive('timepicker', function($parse, $log, $timeout) {
    return {
      restrict: 'E',
      scope: {
        ngModel: '=',
        ngChange: '&',
        required: '@'
      },
      templateUrl: 'bower_components/apform/templates/timepicker.html',
      replace: true,
      link: function($scope){

        $scope.hours = '00';
        $scope.minutes = '00';

        var time;

        $scope.$watch('ngModel', function(newValue){
          if(newValue !== undefined){
            time = newValue.split(':');
            $scope.hours = time[0];
            $scope.minutes = time[1];
          }
        });

        $scope.changeTime = function(){
          $scope.ngModel = $scope.hours+':'+$scope.minutes;
          $timeout($scope.ngChange, 0);
        };
      }
    };
  })

  .directive("checkbox", function() {
    return {
      scope: {},
      require: "ngModel",
      restrict: "E",
      replace: "true",
      template: "<button type=\"button\" ng-style=\"stylebtn\" class=\"btn btn-default\" ng-class=\"{'btn-xs': size==='default', 'btn-sm': size==='large', 'btn-lg': size==='largest', 'checked': checked===true}\">" +
      "<span ng-style=\"styleicon\" class=\"glyphicon\" ng-class=\"{'glyphicon-ok-2': checked===true}\"></span>" +
      "</button>",
      link: function(scope, elem, attrs, modelCtrl) {
        scope.size = "default";
        // Default Button Styling
        scope.stylebtn = {};
        // Default Checkmark Styling
        scope.styleicon = {"width": "10px", "left": "-1px"};
        // If size is undefined, Checkbox has normal size (Bootstrap 'xs')
        if(attrs.large !== undefined) {
          scope.size = "large";
          scope.stylebtn = {"padding-top": "2px", "padding-bottom": "2px", "height": "30px"};
          scope.styleicon = {"width": "8px", "left": "-5px", "font-size": "17px"};
        }
        if(attrs.larger !== undefined) {
          scope.size = "larger";
          scope.stylebtn = {"padding-top": "2px", "padding-bottom": "2px", "height": "34px"};
          scope.styleicon = {"width": "8px", "left": "-8px", "font-size": "22px"};
        }
        if(attrs.largest !== undefined) {
          scope.size = "largest";
          scope.stylebtn = {"padding-top": "2px", "padding-bottom": "2px", "height": "45px"};
          scope.styleicon = {"width": "11px", "left": "-11px", "font-size": "30px"};
        }

        var trueValue = true;
        var falseValue = false;

        // If defined set true value
        if(attrs.ngTrueValue !== undefined) {
          trueValue = attrs.ngTrueValue;
        }
        // If defined set false value
        if(attrs.ngFalseValue !== undefined) {
          falseValue = attrs.ngFalseValue;
        }

        // Check if name attribute is set and if so add it to the DOM element
        if(scope.name !== undefined) {
          elem.name = scope.name;
        }

        // Update element when model changes
        scope.$watch(function() {
          if(modelCtrl.$modelValue === trueValue || modelCtrl.$modelValue === true) {
            modelCtrl.$setViewValue(trueValue);
          } else {
            modelCtrl.$setViewValue(falseValue);
          }
          return modelCtrl.$modelValue;
        }, function() {
          scope.checked = modelCtrl.$modelValue === trueValue;
        }, true);

        // On click swap value and trigger onChange function
        elem.bind("click", function() {
          scope.$apply(function() {
            if(modelCtrl.$modelValue === falseValue) {
              modelCtrl.$setViewValue(trueValue);
            } else {
              modelCtrl.$setViewValue(falseValue);
            }
          });
        });
      }
    };
  })

  .directive('warning', function() {
    return {
      restrict: 'E',
      scope: {
        trans: '@'
      },
      templateUrl: 'bower_components/apform/templates/warning.html',
      replace: true
    };
  })

  .directive('autoFocus', function($timeout) {
    return {
      restrict: 'AC',
      link: function(scope, element) {
        $timeout(function(){
          element[0].focus();
        }, 0);
      }
    };
  })

  .directive('currency', function() {
    return {
      restrict: 'A',
      scope: {
        bindModel:'=ngModel'
      },
      link: function($scope){
        $scope.$watch('bindModel', function(newValue, lastValue){
          if(newValue === undefined){
            $scope.bindModel = '0.00';
          }else{
            var number = parseFloat(newValue);
            if(isNaN(number)){
              $scope.bindModel = lastValue;
            }else{
              $scope.bindModel = String(number.toFixed(2));
            }
          }
        });
      }
    };
  })

  .directive('apinput', function() {
    return {
      restrict: 'E',
      scope: {
        label: '@'
      },
      templateUrl: 'bower_components/apform/templates/apinput.html',
      transclude: true,
      link: function (scope, element, attrs, ctrl, transclude) {
        transclude(function (clone) {
          var tag = clone[1];

          scope.name = tag.getAttribute('name');
          scope.required = tag.getAttribute('ng-required');

          // for input-group forms
          if(!scope.name){
            var newTag = tag.getElementsByTagName("INPUT")[0];

            // if radio-buttons
            if(!newTag){
              newTag = tag.getElementsByTagName("LABEL")[0];
            }

            if(newTag){
              scope.name = newTag.getAttribute('name');
              scope.required = newTag.getAttribute('ng-required');
            }
          }

          if(scope.label === undefined){
            scope.tLabel = scope.name;
          }else{
            scope.tLabel = scope.label;
          }

          // transclude element
          angular.element(element[0].getElementsByTagName("DIV")[1]).append(clone[1]);
        });
      }
    };
  })

  .directive('validate', function ($formService, $compile, $log) {
    return {
      restrict: 'A',
      priority: 1500,
      terminal: true,
      scope: false,
      compile: function(tElement, tAttributes) {
        var validate = tAttributes.validate;
        var model, field, subModel, splittedModel, schemaOptions;

        if(validate){
          splittedModel = validate.split('.');
          if(splittedModel.length >= 3){
            model = splittedModel[0];
            subModel = splittedModel[1];
            field = splittedModel[2];
          }else{
            model = splittedModel[0];
            field = splittedModel[1];
          }
        }else{
          splittedModel = tAttributes.ngModel.split('.');
          model = splittedModel[1];
          field = splittedModel[2];
        }

        if(field) {
          if (!$formService.schemas[model]) {
            return $log.error('No model {' + model + '} defined');
          }

          if(subModel){
            var arrayModel = $formService.schemas[model][subModel];
            if(arrayModel){
              schemaOptions = $formService.schemas[model][subModel].type[0][field];
              if (!schemaOptions) {
                return $log.error('No field {' + '[' + subModel + ']' + '.' +field + '} in schema defined');
              }
            }else{
              var combinedField = subModel + '.' + field;
              schemaOptions = $formService.schemas[model][combinedField];

              if (!schemaOptions) {
                return $log.error('No field {' + combinedField + '} in schema defined');
              }
            }
          }else{
            schemaOptions = $formService.schemas[model][field];
            if (!schemaOptions) {
              return $log.error('No field {' + field + '} in schema defined');
            }
          }

          // Set id & name
          if (!tAttributes.id) {
            tElement.attr('id', field);
          }
          if (!tAttributes.name) {
            tElement.attr('name', field);
          }

          // Set Validations
          if (schemaOptions.required) {
            tElement.attr('ng-required', schemaOptions.required);
          }

          if (schemaOptions.minlength) {
            tElement.attr('ng-minlength', schemaOptions.minlength);
          }

          if (schemaOptions.maxlength) {
            tElement.attr('ng-maxlength', schemaOptions.maxlength);
          }

          if (schemaOptions.match) {
            tElement.attr('ng-pattern', schemaOptions.match);
          }

          if (schemaOptions.min) {
            tElement.attr('min', schemaOptions.min);
          }

          if (schemaOptions.max) {
            tElement.attr('max', schemaOptions.max);
          }
        }

        tElement.removeAttr("validate");
        return {
          pre: function preLink(scope, iElement, iAttrs, controller) {},
          post: function postLink(scope, iElement) {
            $compile(iElement)(scope);
          }
        };
      }
    };
  })

  .directive('aplistinput', function () {
    return {
      restrict: 'E',
      scope: {
        templateUrl: '@',
        type: '@',
        items: '=',
        ngModel: '='
      },
      templateUrl: 'bower_components/apform/templates/aplistinput.html',
      replace: true,
      link: function($scope, element, attrs) {
        $scope.state = 'index';

        var resetForm = function () {
          if($scope.type === 'array'){
            $scope.new = [];
          }else{
            $scope.new = {};
          }
        };

        resetForm(); // initial reset

        var template  = $scope.templateUrl.split('.');
        var modelName = attrs.ngModel;

        $scope.$watch('state', function (newVal) {
          if(newVal === 'index'){
            $scope.$emit(modelName+'.index');
          }
        });

        $scope.getTemplate = function () {
          if($scope.state === 'form'){
            return template[0] + '.form.html';
          }else{
            return template[0] + '.index.html';
          }
        };

        $scope.form = function () {
          resetForm();
          $scope.state = 'form';
        };

        $scope.cancel = function () {
          $scope.state = 'index';
        };

        $scope.save = function () {
          if(!$scope.ngModel){$scope.ngModel = [];}

          if($scope.type === 'array'){
            $scope.ngModel.push($scope.new.array);
          }else{
            if($scope.new.$$edit !== undefined){ // edit
              $scope.ngModel[$scope.new.$$edit] = $scope.new;
            }else{ // new
              $scope.ngModel.push($scope.new);
            }
          }

          $scope.$emit(modelName+'.saved', $scope.new);
          $scope.new = {};
          $scope.state = 'index';
        };

        $scope.edit = function (index) {
          angular.copy($scope.ngModel[index], $scope.new);
          $scope.new.$$edit = index;
          $scope.$emit(modelName+'.edit', $scope.new);
          $scope.state = 'form';
        };

        $scope.destroy = function (index) {
          $scope.$emit(modelName+'.destroyed', $scope.ngModel[index]);
          $scope.ngModel.splice(index, 1);
        };
      }
    };
  })

  .directive('apmodalinput', function ($modal) {
    return {
      restrict: 'E',
      scope: {
        templateUrl: '@',
        type: '@',
        items: '=',
        ngModel: '='
      },
      template: "<div ng-include='templateUrl'></div>",
      replace: true,
      link: function($scope, element, attrs) {

        var template  = $scope.templateUrl.split('.');
        var modelName = attrs.ngModel;

        var ModalFormCtrl = function ($scope, $modalInstance, items) {
          if(items.item) {
            $scope.new = items.item;
          }else{
            $scope.new = {};
          }

          $scope.items = items.items;
          $scope.index = items.index;

          $scope.save = function(onlyfield){
            if(onlyfield){
              $modalInstance.close($scope.new[onlyfield]);
            }else{
              $modalInstance.close($scope.new);
            }
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
        };

        $scope.$on(modelName+'.form', function(event, item){
          $scope.form(item);
        });

        $scope.form = function(model){
          if(!$scope.ngModel){$scope.ngModel = [];}

          if(model !== undefined) { // edit or copy
            var index = $scope.ngModel.indexOf(model);
          }

          var modalForm = $modal.open({
            templateUrl: template[0]+'.form.html',
            controller: ModalFormCtrl,
            resolve: {
              items: function () {
                return {item: angular.copy(model), items: $scope.items, index: index };
              }
            }
          });

          modalForm.result.then(function(item){
            if(!$scope.ngModel){$scope.ngModel = [];}

            if(model !== undefined){ // edit or copy
              if(index > -1){ // on edit
                $scope.ngModel[index] = item;
                $scope.ngModel[index]._index = index;
                $scope.ngModel[index]._edit = true;
              }else{ // on copy
                $scope.ngModel.push(item);
              }
            }else{ // new
              $scope.ngModel.push(item);
            }
            $scope.$broadcast('$$rebind:refreshModalList');
            $scope.$emit(modelName+'.saved', item);
          }, function(){
            $scope.$emit(modelName+'.cancel');
          });
        };

        $scope.copy = function (item) {
          var newItem = angular.copy(item);
          delete newItem.$$hashkey;
          delete newItem._id;
          $scope.form(newItem);
        };

        $scope.destroy = function (item) {
          var index = $scope.ngModel.indexOf(item);
          var todelete = angular.copy($scope.ngModel[index]);
          $scope.ngModel.splice(index, 1);
          $scope.$emit(modelName+'.destroyed', todelete);
        };
      }
    };
  })

  .directive('apformbutton', function (
    $parse, $modal, $log, $state, $rootScope, $formService, $stateParams, $http, $toaster, $translate,
    $location, $window, hotkeys, $socket, $timeout, $stateChange) {
    return {
      restrict: 'E',
      scope: {},
      templateUrl: 'bower_components/apform/templates/apformbutton.html',
      replace: true,
      transclude: true,
      link: function($scope){
        $scope.f = $formService.f;

        if(!$scope.f.redirect){
          $scope.f.redirect = $scope.f.action;
        }

        $stateChange.on(function(todo, toState, toParams){
          if(todo === 'save'){

            var nUrl = toState.url.substring(1, toState.url.length);
            if(toParams !== undefined){
              async.forEachOf(toParams, function(param, key, cb){
                nUrl = nUrl.replace(':'+key, param);
                cb();
              }, function(){
                $scope.f.redirect = nUrl;
                $scope.save(false, false);
              });
            }else{
              $scope.f.redirect = nUrl;
              $scope.save(false, false);
            }
          }
        });


        /**
         * Datenbankabfrage für ein Datensatz
         * @param {id} $stateParams.id - ID des gewünschten Datensatz
         * @success - $scope.thing
         */
        if($stateParams.id){
          $http.get('/'+$scope.f.action+ '/' + $stateParams.id).then(function (res) {
            $scope.f[$scope.f.model] = res.data;
            $rootScope.$emit('dataLoaded', $scope.f);
          });
        }

        var saveEvent = function () {
          $rootScope.$emit('formSaved', $scope.f);
        };

        var cancelEvent = function () {
          $rootScope.$emit('formCanceled', $scope.f);
        };

        var beforeSave = function(cb){
          if(typeof $scope.f.beforeSave === 'function'){
            $scope.f.beforeSave(function(){
              cb();
            });
          }else{
            cb();
          }
        };

        var afterSave = function(cb){
          if(typeof $scope.f.afterSave === 'function'){
            $scope.f.afterSave(function(){
              cb();
            });
          }else{
            cb();
          }
        };

        var createRecord = function (nnew, notredirect) {
          beforeSave(function(){
            $http.post('/'+$scope.f.action, $scope.f[$scope.f.model]).then(function () {
              afterSave(function(){
                saveEvent();
                if(!notredirect){
                  $stateChange.off();
                  if(nnew){
                    $state.go($state.$current, null, { reload: true });
                    $toaster.pop('success','toaster.'+$scope.f.model, 'toaster.saved');
                  }else{
                    if($scope.f.model === undefined){
                      $log.error('formService {model} not defined');
                    }else{
                      $toaster.pop('success','toaster.'+$scope.f.model, 'toaster.saved');
                      $location.path('app/'+$scope.f.redirect);
                    }
                  }
                }
              });
            });
          });
        };

        var updateRecord = function (nnew, notredirect) {
          beforeSave(function() {
            $http.put('/' + $scope.f.action, $scope.f[$scope.f.model]).then(function () {
              afterSave(function() {
                saveEvent();
                if (!notredirect) {
                  $stateChange.off();
                  if (nnew) {
                    $toaster.pop('success', 'toaster.' + $scope.f.model, 'toaster.saved');
                    $location.path('app/' + $scope.f.redirect + '/new');
                  } else {
                    if ($scope.f.model === undefined) {
                      $log.error('formService {model} not defined');
                    } else {
                      $toaster.pop('success', 'toaster.' + $scope.f.model, 'toaster.saved');
                      $location.path('app/' + $scope.f.redirect);
                    }
                  }
                }
              });
            });
          });
        };

        $scope.save = function (nnew, notredirect) {
          if(($scope.f.valid === true) || ($scope.f.valid === undefined)){
            if($stateParams.id){
              updateRecord(nnew, notredirect);
            }else{
              createRecord(nnew, notredirect);
            }
          }else{ // form not valid
            $toaster.pop('error','toaster.form', 'toaster.notvalid');
          }
        };

        $scope.cancel = function () {
          cancelEvent();
        };

        // call save via event
        $rootScope.$on('saveForm', function(err, options){
          $scope.save(options.nnew, options.noredirect);
        });

        $translate(['hotkeys.save', 'hotkeys.savennew', 'hotkeys.print', 'hotkeys.esc']).then(function (translation){
          hotkeys.add('ctrl+s', translation['hotkeys.save'], function(event) {
            event.preventDefault();
            $scope.save();
          });

          hotkeys.add('ctrl+shift+s', translation['hotkeys.savennew'], function(event) {
            event.preventDefault();
            $scope.save(true);
          });

          hotkeys.add('esc', translation['hotkeys.esc'], function () {
            $location.path('app/'+$scope.f.action);
          });
        });

        $scope.back = function(){
          window.history.back();
        };

        $scope.$on('$destroy', function() {
          hotkeys.del('ctrl+s');
          hotkeys.del('ctrl+shift+s');
          hotkeys.del('esc');
          $stateChange.off();
        });

      }
    };
  })

  .filter("passwordLevel", function($passwordService) {
    return function(input, levels) {
      if(angular.isUndefined(levels)) {
        levels = 4;
      }
      var level = Math.ceil($passwordService.getStrength(input) / (100 / levels));
      return "level"+(level==0?1:level);
    };
  })

  .directive("strength", function($filter, $translate) {
    return {
      restrict: "A",
      scope: {},
      require: '^form',
      link: function(scope, elem, attr, formCtrl) {
        var prevClass = '';
        formCtrl.$setValidity('strength', false);

        $translate(['passwords.pw-level1', 'passwords.pw-level2', 'passwords.pw-level3', 'passwords.pw-level4']).then(function (translations) {
          scope.translations = translations;
        });

        function setClass(password, levels) {
          if(prevClass !== ''){
            elem.removeClass(prevClass);
          }

          if(elem.next().prop('tagName') === 'SPAN'){
            elem.next().remove();
          }

          if(password !== ''){
            formCtrl.$setValidity('strength', true);
            var level = $filter('passwordLevel')(password, levels);
            prevClass = "pw-"+level;
            elem.addClass(prevClass);
            var top = elem.height()/2 - 1+'px;';

            // Wenn "Ungenügendes Passwort"
            if (level === 'level1') { formCtrl.$setValidity('strength', false); }
            elem.after('<span class="pw-text" style="top:'+top+'">'+scope.translations['passwords.'+prevClass]+'</span>');
          }else{
            prevClass = '';
          }
        }

        scope.$watch(function() { return attr.strength; }, function(newPassword) {
          scope.password = newPassword;
          setClass(newPassword, scope.levels);
        });
      }
    };
  })

  .directive('match', function() {
    return {
      require: 'ngModel',
      restrict: 'A',
      scope: {
        match: '='
      },
      link: function(scope, elem, attrs, ctrl) {
        scope.$watch(function() {
          var modelValue = ctrl.$modelValue || ctrl.$$invalidModelValue;
          return (ctrl.$pristine && angular.isUndefined(modelValue)) || scope.match === modelValue;
        }, function(currentValue) {
          ctrl.$setValidity('match', currentValue);
        });
      }
    };
  })

  .factory("$passwordService", function() {
    var aspects = {
      minimumLength: {
        min: 8,
        weight: 30,
        /**
         * Make sure the password meets a minimum length, extra credit if longer, 1/2 partial credit if shorter
         * @param {string} password
         * @returns {number}
         */
        strength: function(password) {
          if(password) {
            var length = password.length;
            if(length < aspects.minimumLength.min) {
              length /= 2;
            }
            return length / aspects.minimumLength.min * aspects.minimumLength.weight;
          }
          return 0;
        }
      },
      uppercaseLetters: {
        min: 1,
        weight: 10,
        /**
         * Make sure there are the minimum number of uppercase letters present, partial credit for less than the minimum
         * @param {string} password
         * @returns {number}
         */
        strength: function(password) {
          if(password) {
            var matches = password.match(/[A-Z]/g);
            if(!matches) {
              return 0;
            }
            return matches.length >= aspects.uppercaseLetters.min?
              aspects.uppercaseLetters.weight:
              (matches.length / aspects.uppercaseLetters.min * aspects.uppercaseLetters.weight);
          }
          return 0;
        }
      },
      lowercaseLetters: {
        min: 1,
        weight: 10,
        /**
         * Make sure there are the minimum number of lowercase letters present, partial credit for less than the minimum
         * @param {string} password
         * @returns {number}
         */
        strength: function(password) {
          if(password) {
            var matches = password.match(/[a-z]/g);
            if(!matches) {
              return 0;
            }
            return matches.length >= aspects.lowercaseLetters.min?
              aspects.lowercaseLetters.weight:
              (matches.length / aspects.lowercaseLetters.min * aspects.lowercaseLetters.weight);
          }
          return 0;
        }
      },
      symbols: {
        min: 1,
        weight: 10,
        /**
         * Make sure there are the minimum number of symbols present, partial credit for less than the minimum
         * @param {string} password
         * @returns {number}
         */
        strength: function(password) {
          if(password) {
            var matches = password.match(/[$-/:-?{-~!\"^_`\[\]]/g);
            if(!matches) {
              return 0;
            }
            return matches.length >= aspects.symbols.min?
              aspects.symbols.weight:
              (matches.length / aspects.symbols.min * aspects.symbols.weight);
          }
          return 0;
        }
      },
      numbers: {
        min: 1,
        weight: 10,
        /**
         * Make sure there are the minimum number of numbers present, partial credit for less than the minimum
         * @param {string} password
         * @returns {number}
         */
        strength: function(password) {
          if(password) {
            var matches = password.match(/[0-9]/g);
            if(!matches) {
              return 0;
            }
            return matches.length >= aspects.numbers.min?
              aspects.numbers.weight:
              (matches.length / aspects.numbers.min * aspects.numbers.weight);
          }
          return 0;
        }
      },
      duplicates: {
        max: 3,
        weight: -30,
        /**
         * Penalize for repeated characters (e.g. "aaaaaa"), each character over max counts for full weight
         * @param {string} password
         * @returns {number}
         */
        strength: function(password) {
          if(password) {
            var strength = 0;
            var matches = password.match(new RegExp("(.)\\1{"+aspects.duplicates.max+",}","g"));
            angular.forEach(matches, function(match) {
              strength += (match.length - aspects.duplicates.max) * aspects.duplicates.weight;
            });
            return strength;
          }
          return 0;
        }
      },
      consecutive: {
        max: 3,
        weight: -10,
        /**
         * Penalize for sequential characters (e.g. "abcde" or "54321"), each character over max counts for full weight
         * @param {string} password
         * @returns {number}
         */
        strength: function(password) {
          if(password) {
            var strength = 0;
            var lastChar = 0;
            var inc = true;
            var count = 0;
            for(var i=0;i<=password.length;i++) {
              var charCode = -99;
              if(i < password.length) {
                charCode = password.charCodeAt(i);
              }
              if(charCode == lastChar+1) {
                if(inc) {
                  count++;
                } else {
                  count = 2;
                }
                inc = true;
              } else if(charCode == lastChar-1) {
                if(!inc) {
                  count++;
                } else {
                  count = 2;
                }
                inc = false;
              } else {
                strength += count > aspects.consecutive.max?
                (count - aspects.consecutive.max) * aspects.consecutive.weight:
                  0;
                count = 1;
              }
              lastChar = charCode;
            }
            return strength;
          }
          return 0;
        }
      },
      dictionary: {
        words: [ "hello", "hallo", "asdf", "qwerty", "qwertz", "password", "passw0rd", "bailey",
          "qwerty", "monkey", "letmein", "dragon", "baseball", "iloveyou", "trustno1", "sunshine",
          "master","welcome", "shadow", "ashley", "football", "jesus", "michael", "ninja", "mustang",
          "test", "pussy", "fuck", "ficken", "passwort", "jennifer", "harley", "abc123", "qazwsx"],
        weight: -40,
        /**
         * Penalize for inclusion of dictionary words
         * @param {string} password
         * @returns {number}
         */
        strength: function(password) {
          if(password) {
            var strength = 0;
            angular.forEach(aspects.dictionary.words, function(word) {
              strength += password.indexOf(word) >= 0?
                aspects.dictionary.weight:
                0;
            });
            return strength;
          }
          return 0;
        }
      }
    };

    /**
     * Compute the password's strength
     * @param {string} password
     * @returns {number} the strength percent
     */
    function computeStrength(password) {
      var total = 0;
      var strength = 0;

      angular.forEach(aspects, function(aspect) {
        if(aspect.weight !== 0) {
          total += aspect.weight>0?aspect.weight:0;
          strength += aspect.strength(password);
        }
      });

      return total>0?(strength/total*100):0;
    }

    return {
      aspects: aspects,
      getRawStrength: computeStrength,
      getStrength: function(password) {
        var percent = computeStrength(password);
        return percent>100?100:(percent<0?0:percent);
      },
      isStrongEnough: function(password,minStrength) {
        return computeStrength(password) >= minStrength;
      }
    };
  });
